"""Here are abstract classes used in game."""


class Game:
    tasks = {}
    here = None

    def won(self):
        won = True
        for t in self.tasks.values():
            won = won and t
        return won


class Being(Game):
    def __init__(self, name):
        self.name = name

    def greet(self):
        raise NotImplementedError

    def ask_for(self, what):
        raise NotImplementedError


class Place(Game):
    def __init__(self, name, story):
        self.name = name
        self.story = story
        self.beings = []
        self.places = []

    def visit(self):
        print()
        print(f"################  {self.name.upper()}  ################")
        print(f"{self.story}")
        print()
        for b in self.beings:
            print(f"{b.name}: {b.greet()}")
        print()
        print("Places to go:")
        for p in self.places:
            print(f"- {p.name}")
        print()
        while True:
            w = input("You may ask someone for something or go somewhere:\n\n")
            w = w.lower()
            if w.startswith("ask"):
                s = w.split(" ")
                who = None
                if len(s) > 1:
                    who = w.split(" ")[1]
                what = None
                if len(s) > 2:
                    what = w.split(" ")[2]
                for b in self.beings:
                    if b.name.lower() == who:
                        print(f"{b.name}: {b.ask_for(what)}")
                        print()
                        if self.won():
                            exit(0)
            elif w.startswith("go"):
                where = w.split(" ")[1]
                for p in self.places:
                    if p.name.lower() == where:
                        return p
