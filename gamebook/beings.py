"""Beings being here."""
from random import choice
from gamebook.abstract import Being


class Guide(Being):
    def greet(self):
        return "Hello, stranger. You may ask me for nothing."

    def ask_for(self, what):
        if what == "nothing":
            if ("talk to drunken" in self.tasks
                    and self.tasks["talk to drunken"]
                    and "return to guide" in self.tasks):
                self.tasks["return to guide"] = True
                return "You won! Bye!"
            return "Yes! You did it! Now go, explore the Pub, and return back!"
        else:
            return "Sorry, I do not understand what you mean."


class DrunkenMaster(Being):
    saying = [
        "Buuhm buhm buh",
        "Duh buh bum barimbam",
        "Hehuhmn boh poh koh",
        "Hulalahuha bulabumbu",
        "Ouijivukuivi",
        "Uou ouou uooo ou uuu",
        "Bah blah bumbh bum"]

    def greet(self):
        return choice(self.saying)

    def ask_for(self, what):
        if "talk to drunken" in self.tasks:
            self.tasks["talk to drunken"] = True
        return choice(self.saying)
