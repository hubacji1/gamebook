"""Store of the games."""
from gamebook.abstract import Game, Place
from gamebook.beings import Guide, DrunkenMaster


class ShortGame(Game):
    def __init__(self):
        self.tasks["talk to drunken"] = False
        self.tasks["return to guide"] = False

        s = Place("Square", "It's beautiful being here at this square.")
        s.beings.append(Guide("Buzz"))

        p = Place("Pub", "There is a little to see, but a lot to drink here.")
        p.beings.append(DrunkenMaster("Foo"))
        p.beings.append(DrunkenMaster("Bar"))

        s.places.append(p)
        p.places.append(s)

        self.here = s
